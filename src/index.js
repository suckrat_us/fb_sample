import React from 'react';
import ReactDom from 'react-dom';


import App from '../src/js/app';

import './assets/css/style.css';

ReactDom.render(<App /> ,document.getElementById("app"));