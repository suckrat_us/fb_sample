import React from 'react';
import FacebookLogin from 'react-facebook-login';



export default class HeaderPage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            isLoggedIn: this.props.isLoggedIn,
        }
        this.handleClick = this.handleClick.bind(this)
    }
    responseFacebook = response => {
        this.setState({
          isLoggedIn:true,
          accessToken: response.accessToken
        })
        console.log(this.state.isLoggedIn)
    }
    handleClick = (isLoggedIn) => {

        this.props.onUpdate(this.state.isLoggedIn)
        console.log(isLoggedIn)
        
    }
    render() {
        let isLoggedIn;
        if(!this.state.isLoggedIn && !this.props.isLoggedIn){
            isLoggedIn = false
        }else{
            isLoggedIn = true
        }



        return(
            <div className="wrapper">
            
                <nav>
                    <ul className="main_menu">

                        <li><h2>Generic</h2></li>
                        { !isLoggedIn &&(
                        <li>

                                <FacebookLogin
                                 appId="280617292561959"
                                 autoLoad={true}
                                 fields="name,email,picture"
                                 scope="public_profile,ads_management,ads_read,business_management"
                                 callback={this.responseFacebook}
                                 textButton="Login"
                                 size='small'
                                 onClick={this.handleClick.bind(this, this.state.isLoggedIn)}
                                    >
                                       

                                
                                </FacebookLogin>

                            </li>
                        )
                        }
                        { isLoggedIn &&(
                        <li>
                               <FacebookLogin
                                 appId="280617292561959"
                                 autoLoad={true}
                                 fields="name,email,picture"
                                 scope="public_profile,ads_management,ads_read"
                                 callback={this.responseFacebook}
                                 textButton="Logout"
                                 size='small'
                                 onClick={this.handleClick.bind(this, this.state.isLoggedIn)}

                                    >
                                       

                                
                                </FacebookLogin>

                        </li>
                        )
                        }

                    </ul>
                </nav>
            </div>

        )
    }

}