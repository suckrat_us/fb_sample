import React from 'react';
import {Grid, Row,Col,Button} from 'react-bootstrap';
import FacebookLogin from 'react-facebook-login';
import Modal from 'react-responsive-modal';
import {TabContent, TabPane, Nav, NavItem, NavLink,} from 'reactstrap';
import { Search} from 'semantic-ui-react';
import _ from 'lodash';
import classnames from 'classnames';
import axios from 'axios';

import HeaderPage from '../js/components/header';
import '../assets/css/style.css' 

{/*use custom API*/}
const Api = "https://some-api.com/api/";


 export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      id: null,
      accessToken:"",
      login: false,
      isLoggedIn: false
        };
  }

  responseFacebook = response => {
    this.setState({
      isLoggedIn:true,
      accessToken: response.accessToken
    })

    console.log(this.state.accessToken);
  }
  onUpdate = (isLoggedIn) => {
    this.state.isLoggedIn = isLoggedIn;
 
  }

  render() {
    let isLoggedIn = this.state.isLoggedIn;
    return (
      <div>
        <HeaderPage isLoggedIn={this.state.isLoggedIn} onUpdate={this.onUpdate}/>
      { !isLoggedIn && (
      <div className="Login">
        <div className="login-color">
        <div className="fast-login">
        <div className="login-welcome">
        <h1>Welcome</h1>
        </div>
        <h2>Please login</h2>
        <div className="login-btn-cont">
        </div>
        <Button>            
          <FacebookLogin
                appId="280617292561959"
                autoLoad={false}
                fields="name,email,picture"
                scope="public_profile,ads_management,ads_read,business_management"
                callback={this.responseFacebook}
            />
            </Button>

          </div>
        </div>

      </div>
      )
    }
    { isLoggedIn && (
      <MainTable accessToken={this.state.accessToken}/>
    )}
    </div>
    );
  }
}




class MainTable extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      accessToken: this.props.accessToken,
      data: false,
      business_data:false,
      business_id: false,
      account_id:null,
      account_i: false,
      campaing_id: null,
      campaing_data: false,
      set_id:null,
      set_data: false,
      add_id:null,
      add_data: false,
      acc:false,
      id:null,
      load:false
    }
    this.getCampaing = this.getCampaing.bind(this);
    
  }
  componentDidMount() {
    this.getBusiness();
    
  }
  componentDidUpdate(){
  }
  updateData(){

    fetch(Api+"getAdAccounts?access_token=" + this.state.accessToken + "&business_id="+ this.state.business_id)
    .then(response => response.json())
    .then(data => this.setState({data }));


    if(this.state.data.name === null ){
      this.state.load = false
      console.log("1")
    }else{
      this.state.load = true
    }

    }
  getBusiness(){
    fetch(Api+"getBusinessAccounts?access_token=" + this.state.accessToken)
    .then(response => response.json())
    .then(business_data => this.setState({business_data }));
  }
  getCampaing() {
    fetch(Api+"getCampaigns?account_id="+this.state.account_i+"&access_token="+this.state.accessToken)
    .then(response => response.json())
    .then(campaing_data => this.setState({campaing_data }));
  }
  getSets() {
    fetch(Api+"getAdSets?campaign_id="+this.state.campaing_id+"&access_token="+this.state.accessToken)
    .then(response => response.json())
    .then(set_data => this.setState({set_data }));

  }
  getAdds() {
    const options = {
      method: 'get',
      headers: {
          "Access-Control-Request-Headers": "*",
          "Access-Control-Request-Method": "*"
    },
  }
    fetch(Api + "getAds?adset_id="+this.state.set_id+"&access_token="+this.state.accessToken, options)
    .then(response => response.json())
    .then(add_data => this.setState({add_data }));
  }
  onUpdateBusiness = (id) => {
    this.state.business_id = id;
    this.updateData();

  }
  onUpdate = (id) => {
    this.state.account_i = id;
    this.getCampaing();
 
  }
  onUpdateSet = (id) =>{

    this.state.campaing_id = id;
    this.getSets();
    console.log(this.state.campaing_id)
  }
  onUpdateAdd = (id) =>{
    this.state.set_id = id;
    this.getAdds();
    console.log(this.state.set_id)
  }

  render() {
    let error = this.state.data.paging
    let campaign = this.state.campaing_data.paging
    let set = this.state.set_data.paging
    let add = this.state.add_data.paging
    let bus = this.state.business_data.paging
    

    return(
      <div className="main">
      <Grid>
        <Row className="show-grid">
        <Col className="column" sm={6} md={2}>
          <p className="name">Бизнесс аккаунт</p>
          {
              bus === undefined && (
                <ul className="coll">
                  <p>Выберите Бизнесс акаунт</p>
                  </ul>
              )
            }
            {
              bus && (
                <ul className="coll">
                 {this.state.business_data.data.map((data,key) => <ItemBusiness key={key} data={data} onUpdate={this.onUpdateBusiness} ></ItemBusiness>)} 
                  </ul>
              )
            }
          </Col>
          <Col className="column" sm={6} md={2}> 
          <p className="name">Рекламные Аккаунты</p>


          {
            error === undefined &&(
              <ul className="coll">
                <p>Выберите Рекламный Аккаунт</p>
              </ul>
            )
          }
          {
            error && (
              <ul className="coll">
                 {this.state.data.data.map((data,key) => <Item key={key} data={data} onUpdate={this.onUpdate}></Item>)} 
              </ul>
            )
          }
          </Col>
          <Col className="column" sm={6} md={3}>
          <p className="name">Рекламные Кампании</p>
          {
              campaign === undefined && (
                <ul className="coll">
                  <p>Выберите Аккаунт</p>
                  </ul>
              )
            }
            {
              campaign  && (
                <ul className="coll">
                 {this.state.campaing_data.data.map((campaign_data,key) => <ItemCampaign key={key} data={campaign_data} accessToken={this.state.accessToken} accountId={this.state.account_i} onUpdateSet={this.onUpdateSet} ></ItemCampaign>)} 
                  </ul>
              )
            }

          </Col>
          <Col className="column" sm={6} md={3}>
          <p className="name">Группы обьявлений</p>

          {
              set === undefined && (
                <ul className="coll">
                  <p>Выберите Кампанию</p>
                  </ul>
              )
            }
            {
              set && (
                <ul className="coll">
                                 {this.state.set_data.data.map((data,key) => <ItemSet key={key} data={data} onUpdateAdd={this.onUpdateAdd}></ItemSet>)} 

                  </ul>
              )
            }
          </Col>
          <Col className="column" sm={6} md={2}>
          <p className="name">Обьявления</p>
          {
              add === undefined && (
                <ul className="coll">
                  <p>Выберите обьявление</p>
                  </ul>
              )
            }
            {
              add && (
                <ul className="coll">
                 {this.state.add_data.data.map((data,key) => <Item key={key} data={data} ></Item>)} 
                  </ul>
              )
            }
          </Col>
        </Row>
      </Grid>
      <div className="footer-row"></div>

    </div>
    )
  }
}

class ItemBusiness extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      id:null
    }
  }
  handleClick = (id) => {

    this.state.id = id
    this.props.onUpdate(this.state.id)
    
}
  render() {
    return(
      <li
      onClick={this.handleClick.bind(this, this.props.data.id)} >
        <p>Название:{this.props.data.name}</p>
      </li>
    )
  }
}
class Item extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      id:null
    }
  }
  handleClick = (id) => {

    this.state.id = id
    this.props.onUpdate(this.state.id)
    
}
  render() {
    return(
      <li
      onClick={this.handleClick.bind(this, this.props.data.id)} >
        <p>Название:<br></br>{this.props.data.name}</p>
      </li>
    )
  }
}
class ItemSet extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      id:null
    }
  }
  handleClick = (id) => {

    this.state.id = id
    this.props.onUpdateAdd(this.state.id)
    
}
  render() {
    return(
      <li
      onClick={this.handleClick.bind(this, this.props.data.id)} >
        <p>Название:{this.props.data.name}</p>
      </li>
    )
  }
}
class ItemCampaign extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      id:this.props.data.id,
      open: false,
      number: 0,
      name:"",
      route: "",
      selectedFile:null,
      selectedPhoto:null,
      loaded: 0,
      activeTab: '1',
      search:undefined,
      accessToken:this.props.accessToken,
      account_id: this.props.accountId,
      name: undefined,
      text:null,
      target_name:undefined,
      target_id: undefined,
      interest_name: " - ",
      interest_id: undefined,
      state_status: false

    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }
handleClick = (id) => {

    this.state.id = id
    this.props.onUpdateSet(this.state.id)
    
}

componentDidMount() {
  this.getData();
}
handleInputChange(event) {
  const target = event.target;
  const name = target.name;
  const value = target.value;


  this.setState({
    [name]: value,
  });
  console.log(this.state.name,this.state.number,this.state.route)
}

onOpenModal = () => {
  this.setState({ 
    open: true,


   });
};

handleselectedFile = event => {
  const target = event.target;
  const name = target.name;
  this.setState({
    [name]: event.target.files[0],
    loaded: 0,

  })
}
handleUpload = () => {
  const data = new FormData()
  data.append('file', this.state.selectedFile, this.state.selectedFile.name,this.state.number,this.state.name)

  axios
    .post(Api, data, {
      headers:{
        'Accept': 'application/json',
        'Content-Type': 'application/json',      }
    })
    .then(res => {
      console.log(res.statusText)
    })

}
duplicateCampaignHand = () => {
  const data = new FormData()
  data.append('access_token',this.state.accessToken);
  data.append('data', this.state.selectedPhoto);
  data.append('new_name', this.state.name);
  data.append('text', this.state.text);
  data.append('campaign_id', this.state.id)
  data.append('interest_name', this.state.interest_name);
  data.append('interest_id',this.state.interest_id);
  fetch(Api+"duplicateAndChangeByHands", {
  method: 'POST',
  body:{data:this.state.selectedPhoto}
});
console.log(data)


}
duplicateCampaign = () => {
  console.log(this.state.accessToken)
  const data = new FormData()
  data.append('data', this.state.selectedFile);
  data.append('number', this.state.number);
  data.append('id', this.state.id);

  fetch(Api+"duplicate", {
  method: 'POST',
  body:data
});
console.log("closed")

}
onCloseModal = () => {
  this.setState({ 
    open: false,
    number: 0,
    name:"",
    route: "",
    selectedFile: null
   });

};

toggle(tab) {
  if (this.state.activeTab !== tab) {
      this.setState({
          activeTab: tab
      });
  }
}
searchData = () =>{
  this.state.search_data = undefined
  fetch("https://graph.facebook.com/v3.2/"+this.props.accountId+"/targetingsearch?access_token="+this.state.accessToken+"&q="+this.state.value,{
    method:'GET'
    })
  .then(response => response.json())
  .then(search_data => this.setState({search_data }));
  console.log(this.state.search_data)
}
getData = () => {
  fetch("https://graph.facebook.com/v3.2/"+this.props.accountId+"/targetingbrowse?access_token="+this.state.accessToken,{
    method:'GET'
    })
  .then(response => response.json())
  .then(search => this.setState({search }));

}
handleInputChange(event) {
  const target = event.target;
  const name = target.name;
  const value = target.value;


  this.setState({
    [name]: value,
  });
}
searchUpdateData = (event) => {



  const target = event.target;
  const name = target.name;
  const value = target.value;


  this.setState({
    [name]: value,
  });


} 
onUpdate = (name,id) => {
  this.state.interest_name = name;
  this.state.interest_id = id;

}
onUpdateTarget = (name,id) => {
  this.state.interest_name = name;
  this.state.interest_id = id;

}

toggleState = () => {
  this.setState({
    state_status: !this.state.state_status
  })

  }


resetComponent = () => this.setState({ isLoading: false, results: [], value: '' })

handleResultSelect = (e, { result }) => this.setState({ value: result.raw_name })

handleSearchChange = (e, { value }) => {
  this.setState({ isLoading: true, value })

  setTimeout(() => {
    if (this.state.value.length < 1){

    this.setState({
      value:undefined,
      search_data:undefined
    })
    return this.resetComponent()
  }

    const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
    const isMatch = result => re.test(result.raw_name)
    this.searchData();

    console.log(this.state.search.data)

    this.setState({
      isLoading: false,
      results: _.filter(this.state.search, isMatch),
    })
  }, 300)
}
  render() {
    const { open } = this.state;
    const { isLoading, value, results } = this.state
    const {accessToken} =this.state

    return(
      <li
       >
        <p onClick={this.handleClick.bind(this, this.props.data.id)}>Название:{this.props.data.name}</p>
        <Button bsStyle="info" bsSize='small' onClick={this.onOpenModal}>Duplicate</Button>
        <Modal 
          open={open} 
          onClose={this.onCloseModal}
          accessToken={accessToken}
          center
          >
           <Nav tabs size='' id="project-tab">
              <NavItem>
                  <NavLink className={classnames({active: this.state.activeTab === '1'})} onClick={() => {
                      this.toggle('1');
                  }}>
                      Из файла
                  </NavLink>
              </NavItem>
              <NavItem>
                  <NavLink className={classnames({active: this.state.activeTab === '2'})} onClick={() => {
                      this.toggle('2');
                  }}>
                      Вручную
                  </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                <div className="modal-content">
      <div className="header">
      <h2>Дублировать Кампанию из папки</h2>

      </div>
      <div className="main-content">
        <form action="" >
            <p>
              <label htmlFor="number">
                Введите колличество копий
                <input type="number" name="number" value={this.state.number} onChange={this.handleInputChange} />
              </label>
            </p>
            <hr/>

            <p>
              <label htmlFor="route">
                Выберите архив
                <input className="text-field" type="file" name="selectedFile"  onChange={this.handleselectedFile} />
              </label>
            </p>
            <hr/>
          </form>
      </div>
      <div className="footer">
        <Button bsStyle="success" onClick={this.duplicateCampaign} >Duplicate</Button>

        <Button bsStyle="danger" className="close-button" onClick={this.onCloseModal}>Сlose</Button>
      </div>
  </div>

                </TabPane>
                <TabPane tabId="2">
                <div className="modal-content">
                       <div className="header">  
                <h2>Дублировать Кампанию вручную</h2>

      </div>
      <div className="main-content">
        <form action="" >
            <p classname="style-head">
                Введите интересы
                </p>
                <div className="search-block-container">
                <p>Интересы: {this.state.interest_name}</p>
                <Search className="search-text-filed"
                  loading={isLoading}
                  onResultSelect={this.handleResultSelect}
                  onSearchChange={_.debounce(this.handleSearchChange, 500, { leading: true })}
                  results={results}
                  value={value}
                  {...this.props}
                  >
              </Search>

             
             
              </div> 
           <div> { !this.state.state_status  && (
                <button className="search-button" onClick={ this.toggleState.bind(this)} >Show Target</button>)
              }
              { this.state.state_status && (
                <button className="search-button" onClick={ this.toggleState.bind(this)} >close Target</button>)

              }
              </div>
            <div >
          { this.state.search_data != undefined && (
            <div className="search-container">
              <p>Search</p>
              <ul >
              {this.state.search_data.data.map((search_data,key) => <SearchDataComp key={key} data={search_data} onUpdate={this.onUpdate}></SearchDataComp>)}

              </ul>
              </div>
          )
          }
          { this.state.state_status && (
            <div className="search-container-target">

            <p>Target</p>
              <ul>
              {this.state.search.data.map((search,key) => <TargetDataComp key={key} data={search} onUpdateTarget={this.onUpdateTarget}></TargetDataComp>)}

              </ul></div>


          )}
            </div>

                      <hr/>
                <p classname="style-head">
            Введите Название

            </p>
            <label  htmlFor="name">
                <input className="text-field" type="text" name="name"value={this.state.name} onChange={this.handleInputChange} />
              </label>
              <hr></hr>
            <p classname="style-head">
            Введите текст Обьявления

            </p>
            <label  htmlFor="name">
                <input className="text-field" type="text" name="text"value={this.state.text} onChange={this.handleInputChange} />
              </label>
              <hr></hr>
            <p>
              <label htmlFor="route">
                Выберите фото
                <input className="text-field" type="file" name="selectedPhoto"  onChange={this.handleselectedFile} />
              </label>
            </p>
            <hr/>
          </form>
      </div>
      <div className="footer">
        <Button bsStyle="success" onClick={this.duplicateCampaignHand} >Duplicate</Button>

        <Button bsStyle="danger" className="close-button" onClick={this.onCloseModal}>Сlose</Button>
      </div>
  </div>
                </TabPane>
            </TabContent>
    
        
        
        </Modal>
      </li>
    )
  }
}
class SearchDataComp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      name: '',
      id: ''
    };

  }

  handleClick = (name,id) => {

    this.state.name = name
    this.state.id = id
    this.props.onUpdate(this.state.name,this.state.id)
  }
    
  render() {
    return(
      <li 
      className="search-data-component"
      onClick={this.handleClick.bind(this, this.props.data.name,this.props.data.id)}
      >{this.props.data.name}</li>
      )
  }
}
class TargetDataComp extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      name: '',
      id:''
    };

  }

  handleClick = (name,id) => {

    this.state.name = name
    this.state.id = id
    this.props.onUpdateTarget(this.state.name,this.state.id)
  }
    
  render() {
    return(
      <li 
      className="search-data-component"
      onClick={this.handleClick.bind(this, this.props.data.name,this.props.data.id)}
      >{this.props.data.raw_name}</li>
      )
  }
}


const AddAcount = {
  "data":[{"name":"juventalab","id":"act_817241695136985"}],
  "paging":{
    "cursors":{"before":"QVFIUjRXOS1tTENqU0llMVVyemlFX0dRVjQwUUVJRGp6NHNnRlJQTmFONWhzTW5iUWljWGotRmVhN1ZARN3czbHRCVUU2dWpXRDBfcURBRVNVT2pCN1Y5WjBR","after":"QVFIUjRXOS1tTENqU0llMVVyemlFX0dRVjQwUUVJRGp6NHNnRlJQTmFONWhzTW5iUWljWGotRmVhN1ZARN3czbHRCVUU2dWpXRDBfcURBRVNVT2pCN1Y5WjBR"}}
}

const Set = {
  "data":[{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},{"name":"куку","id":"23843110404970225"},],"paging":{"cursors":{"before":"MjM4NDMxMTA0MDQ5NzAyMjUZD","after":"MjM4NDMxMTA0MDQ5NzAyMjUZD"}}
}
const Ads = {"data":[{"name":"Default Name - Охват","id":"23843110404990225"}],"paging":{"cursors":{"before":"MjM4NDMxMTA0MDQ5OTAyMjUZD","after":"MjM4NDMxMTA0MDQ5OTAyMjUZD"}}}